package main

import (
	"container/list"
	"fmt"
)

type actioner interface {
	show() string
	deleteRecord(int)
}

type node struct {
	nodeName string
	idList   []int
}

type topoNode struct {
	relatedNode *node
}

func (n *node) show() {
	fmt.Print("nodeName:")
	fmt.Println(n.nodeName)
	fmt.Print("idList:")
	fmt.Println(n.idList)
}

func (n *node) deleteRecord(target int) {
	for i := 0; i < len(n.idList); i++ {
		if n.idList[i] == target {
			n.idList = append(n.idList[:i], n.idList[i+1:]...)
		}
	}
}

type graph struct {
	root           *node
	allNodes       []*node
	adjacentMatrix [][]bool
	preMap         map[*node]*node
}

type topoGraph struct {
	topoNodes      []*topoNode
	adjacentMatrix [][]bool
	relationMap    map[*node]*topoNode
}

/***
 *** relation: the connection relation, every two
 *** adjacent values means there is a connection.
 *** e.g. a relation is [0, 1, 0, 2], means that node0 is
 *** connected to node1 and node0 points to node1; node0
 *** is connected to node2 and node0 points to node2.
 ***/
func (g *topoGraph) constructTopoGraph(topoNodes []*topoNode, relation []int) {
	g.topoNodes = topoNodes
	size := len(topoNodes)
	g.adjacentMatrix = make([][]bool, size)
	for i := range g.adjacentMatrix {
		g.adjacentMatrix[i] = make([]bool, size)
	}
	for i := 0; i < len(relation); i += 2 {
		g.adjacentMatrix[relation[i]][relation[i+1]] = true
	}
}

func (g *graph) constructDagGraph(root *node, allNodes []*node, relation []int) {
	g.root = root
	g.allNodes = allNodes
	size := len(allNodes)
	g.adjacentMatrix = make([][]bool, size)
	for i := range g.adjacentMatrix {
		g.adjacentMatrix[i] = make([]bool, size)
	}
	for i := 0; i < len(relation); i += 2 {
		g.adjacentMatrix[relation[i]][relation[i+1]] = true
	}

	g.preMap = make(map[*node]*node)
	g.constructPreMap()
}

/***
 ***  record every node's pre node
 ***  there is a bug, can only record one pre node,
 ***  in fact, there can be more than one pre node
 ***  in DAG
 ***/
func (g *graph) constructPreMap() {
	visited := make([]bool, len(g.allNodes))
	for i := 0; i < len(visited); i++ {
		if !visited[i] {
			g.constructPreMapHelper(g.allNodes[i], visited)
		}
	}
}

// likely it can be done only by adjacent matrix
func (g *graph) constructPreMapHelper(n *node, visited []bool) {
	index := g.nodeIndex(n)
	visited[index] = true
	for i := 0; i < len(visited); i++ {
		if index != i && !visited[i] && g.adjacentMatrix[index][i] {
			g.preMap[g.allNodes[i]] = n
			g.constructPreMapHelper(g.allNodes[i], visited)
		}
	}
}

// DAG
func (g *graph) search(target int) topoGraph {
	var topoG = topoGraph{}
	topoG.relationMap = make(map[*node]*topoNode)
	var topoRelation = make([]int, 0)
	var topoNodes = make([]*topoNode, 0)
	g.searchHelper(target, g.root, &topoNodes, &topoRelation, topoG)
	topoG.constructTopoGraph(topoNodes, topoRelation)
	return topoG
}

func (g *graph) searchHelper(target int, n *node, topoNodes *[]*topoNode, topoRelation *[]int, topoG topoGraph) {
	nIndex := g.nodeIndex(n)
	size := len(g.allNodes)

	if isConcluded(target, n) {

		n.show()
		fmt.Println("target record", target)
		newNode := topoNode{}
		newNode.relatedNode = n
		topoG.relationMap[n] = &newNode
		*topoNodes = append(*topoNodes, &newNode)
		// process here
		// add topoRelation
		_, has := g.preMap[n]
		if has {
			sIndex := topoNodeIndex(n, *topoNodes, topoG)

			tIndex := topoNodeIndex(g.preMap[n], *topoNodes, topoG)

			*topoRelation = append(*topoRelation, sIndex)
			*topoRelation = append(*topoRelation, tIndex)
		}

		for i := 0; i < size; i++ {
			if g.adjacentMatrix[nIndex][i] {
				g.searchHelper(target, g.allNodes[i], topoNodes, topoRelation, topoG)
			}
		}
	}

}

func isConcluded(target int, n *node) bool {
	for i := 0; i < len(n.idList); i++ {
		if n.idList[i] == target {
			return true
		}
	}
	return false
}

func (g *graph) nodeIndex(n *node) int {
	for index := 0; index < len(g.allNodes); index++ {
		if g.allNodes[index] == n {
			return index
		}
	}
	return -1
}

func topoNodeIndex(n *node, topoNodes []*topoNode, topoG topoGraph) int {
	topoN := topoG.relationMap[n]
	for index := 0; index < len(topoNodes); index++ {
		if topoNodes[index] == topoN {
			return index
		}
	}
	return -1
}

func (topoG topoGraph) topoProcess(target int) {

	queue := list.New()
	// l.PushBack(1)
	// i1 := l.Front()
	// l.Remove(i1)

	indegree := make([]int, len(topoG.topoNodes))

	for i := 0; i < len(topoG.topoNodes); i++ {
		for j := 0; j < len(topoG.topoNodes); j++ {
			if topoG.adjacentMatrix[i][j] {
				indegree[j]++
			}
		}
	}

	for i := 0; i < len(indegree); i++ {
		if indegree[i] == 0 {
			queue.PushBack(topoG.topoNodes[i])
		}
	}

	fmt.Println("================================")

	for queue.Len() != 0 {
		tempNode := queue.Front()
		queue.Remove(tempNode)

		// fmt.Println(tempNode.Value)
		tempN, ok := tempNode.Value.(*topoNode)
		if ok {
			tempNodeIndex := topoNodeIndex(tempN.relatedNode, topoG.topoNodes, topoG)
			fmt.Println("delete record")
			// execute delete operation
			(*tempN.relatedNode).deleteRecord(target)
			(*tempN.relatedNode).show()
			// decrease in degree
			for i := 0; i < len(indegree); i++ {
				if topoG.adjacentMatrix[tempNodeIndex][i] {
					indegree[i]--
					if indegree[i] == 0 {
						queue.PushBack(topoG.topoNodes[i])
					}
				}
			}
		}
	}
}

func main() {
	// init nodes
	var nodeA = node{}
	var nodeB = node{}
	var nodeC = node{}
	var nodeD = node{}
	var nodeE = node{}
	var nodeF = node{}
	var nodeG = node{}
	var nodeH = node{}

	nodeA.idList = []int{1, 2, 3}
	nodeA.nodeName = "NodeA"

	nodeB.idList = []int{1, 2, 3, 4}
	nodeB.nodeName = "NodeB"

	nodeC.idList = []int{2, 3}
	nodeC.nodeName = "NodeC"

	nodeD.idList = []int{2}
	nodeD.nodeName = "NodeD"

	nodeE.idList = []int{1, 7}
	nodeE.nodeName = "NodeE"

	nodeF.idList = []int{2, 6}
	nodeF.nodeName = "NodeF"

	nodeG.idList = []int{5}
	nodeG.nodeName = "NodeG"

	nodeH.idList = []int{6}
	nodeH.nodeName = "NodeH"

	allNodes := []*node{&nodeA, &nodeB, &nodeC, &nodeD, &nodeE, &nodeF, &nodeG, &nodeH}
	relation := []int{0, 1, 0, 2, 1, 3, 1, 4, 2, 5, 2, 6}

	// init graph
	var g graph = graph{}
	g.constructDagGraph(&nodeA, allNodes, relation)

	topoG := g.search(2)
	fmt.Println("============")
	for i := 0; i < len(topoG.topoNodes); i++ {
		fmt.Println(*topoG.topoNodes[i].relatedNode)
	}

	// topoG is ok
	// pool := NewPool(128)

	topoG.topoProcess(2)

	for {
	}

}

/*
type Pool struct {
	work chan func()   // task
	sem  chan struct{} // number
}

func NewPool(size int) *Pool {
	return &Pool{
		work: make(chan func()),
		sem:  make(chan struct{}, size),
	}
}

func (p *Pool) NewTask(task func()) {
	select {
	case p.work <- task:
	case p.sem <- struct{}{}:
		go p.worker(task)
	}
}

func (p *Pool) worker(task func()) {
	defer func() { <-p.sem }()
	for {
		task()
		task = <-p.work
	}
}
*/
