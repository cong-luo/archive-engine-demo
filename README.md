# Archive Engine Demo

A small demo about database archive engine. It use topology and muti-thread to enhance the efficiency of archive engine.

### Design Background

Traditional archive engine method is too slowly to handle massive data. Here we use topological sorting and Muti-thread to increase the efficiency.

### Design Idea

Use an example to illustrate, below is DAG, you can treat each node as a table. 

![OriginalGraph](https://gitlab.com/cong-luo/archive-engine-demo/-/raw/main/graph/OriginalGraph.png)

If we want to delete the record on nodeA, we can't do that directly, because it is associated with other nodes. We should firstly delete nodeB and nodeC. Similarly, we cant delete the records on these two nodes derectly, they are also associated with other nodes, nodeD etc.

So, we should firstly delete the records on these nodes whose indegree is 0. Here is nodeD, nodeF, etc.

To complete the above ideas, we use topological sorting. After determining the node to be deleted, we reconstruct a topo graph about this record and use topological sorting idea to delete the records successively. Use an example to illustrate, if we want to delete the record 2, then we can construst too Graph as below.

![TopoGraph](https://gitlab.com/cong-luo/archive-engine-demo/-/raw/main/graph/TopoGraph.png)

At the same time, we can note that: if some nodes's indegree are all 0, we can delete these records at the same time. So we also Muti-thread tech to increase this process.

### Things Todo

multi-threading part. 



